import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json

# Use the application default credentials
cred = credentials.ApplicationDefault()
firebase_admin.initialize_app(cred, {
    'projectId': 'ncr-global-hackathon-2019',
})

# initialize
def init_data(collection_id, company_id, company_info, items, purchased_list, sold_list, stock_list, price_list):
    doc_ref = firestore.client().collection(collection_id).document(company_id)
    doc_ref.set(company_info)
    for i in range(len(items)):
        items_collection = doc_ref.collection('items')
        item_doc = items_collection.document(items[i])
        item_doc.set({'name': items[i]})
        item_data = item_doc.collection('data')
        for date in range(len(sold_list[0])):
            item_data.document(str(date)).set({
                'date': date,
                'purchased': purchased_list[i][date],
                'sold': sold_list[i][date],
                'stock': stock_list[i][date],
                'price': price_list[i][date]
            })


# data is a date : value pair
def set_data(collection_id, company_id, item_id, date, purchased, sold, stock, price):
    firestore.client().collection(collection_id).document(company_id).\
        collection('items/'+item_id+'/data').document(str(date)).set({
        'date': date,
        'purchased': purchased,
        'sold': sold,
        'stock': stock,
        'price': price
    })

# dates is a list of string
def get_data(collection_id, company_id, item_id, date_range):
    docs_ref = firestore.client().collection(collection_id).document(company_id).\
        collection('items').document(item_id).collection('data').\
        where('date', '>=', date_range[0]).where('date', '<=', date_range[1])
    docs = docs_ref.stream()
    dates = []
    purchased_list = []
    sold_list = []
    stock_list = []
    price_list = []
    for doc in docs:
        doc_dict = doc.to_dict()
        dates.append(doc_dict['date'])
        purchased_list.append(doc_dict['purchased'])
        sold_list.append(doc_dict['sold'])
        stock_list.append(doc_dict['stock'])
        price_list.append(doc_dict['price'])
    return (dates, purchased_list, sold_list, stock_list, price_list)

def get_company_info(collection_id, company_id):
    return firestore.client().document(collection_id+'/'+company_id).get().to_dict()

def get_items_list(collection_id, company_id):
    item_stream = firestore.client().collection(collection_id+'/'+company_id+'/items').stream()
    return [item.id for item in item_stream]

# 0 or 1(only 2 items)
def return_future_prediction(item_id):
    doc_ref = firestore.client().collection(u'Walmart(dummy-data)').document(item_id)
    return doc_ref.to_dict()['future_prediction']
    # docs = users_ref.stream()
    # for doc in docs:
    #     doc_dict = doc.to_dict()
    #     return doc_dict['future_prediction']

# Same 0 or 1
def return_graph_data():
    users_ref = firestore.client().collection(u'Walmart(dummy-data)')
    docs = users_ref.stream()
    for doc in docs:
        doc_dict = doc.to_dict()
        preds = doc_dict['preds']
        test_data = doc_dict['test_data']
    return preds, test_data

if __name__=='__main__':
    print ('testing')
    company_id = 'company-1'
    company_info = {'company': 'WelMart',
                    'username': 'user',
                    'email': 'NA',
                    'first': 'Mike',
                    'last': 'Hu',
                    'address': 'NA',
                    'city': 'Atlanta',
                    'country': 'US',
                    'zipcode': '30332',
                    'about': 'NA'}
    collection_id = 'test-collection-5'
    items = ['coke']
    sold_list = [[1, 4, 3, 2]]
    purchased_list = [[3, 2, 4, 4]]
    stock_list = [[9, 3, 5, 3]]
    price_list = [[4, 3, 4, 2]]
    init_data(collection_id, company_id, company_info, items, purchased_list, sold_list, stock_list, price_list)
    set_data(collection_id, company_id, items[0], -2, 12, 7, 5, 8)
    got_date, got_purchase, got_sold, got_stock, got_price = get_data(collection_id, company_id, items[0], (-2, 4))
    print (got_price)
    print (got_date)
    print (got_stock)
    print (get_company_info(collection_id, company_id))
    print (get_items_list(collection_id, company_id))