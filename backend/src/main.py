from flask import Flask, request, jsonify, Response
from flask import json
from flask_cors import CORS
import os
import logic
import datagen
import firebase_access
import pandas
import numpy


app = Flask(__name__)
CORS(app, support_credentials=True)

#collection_id = 'test-collection-4'
collection_id = os.environ.get("COLLECTION")

# endpoints
@app.route("/health/", methods=["GET"])
def hello():
#business logic
    return "OK"

#########################################################################################################
#Data functions
#########################################################################################################

@app.route("/customer/<customer_id>/runprediction", methods=["GET"])
def predict_data(customer_id):
    preds_list, test_data_list, future_prediction_list = [], [], []
    try:
        customerId = str(customer_id)
    except:
        return Response(status=400, mimetype="application/json")
    company_info = firebase_access.get_company_info(collection_id, customerId)
    if (company_info != None):
        date_range = (1, 999)
        item_list = firebase_access.get_items_list(collection_id, customerId)                       #get list of items for given customer
        for item in item_list:                              
            dates, purchased_list, sold_list, stock_list, price_list = firebase_access.get_data(collection_id, customer_id, item, date_range)                 #for each item in item list get sold_list
            dic = {}
            # dic['dates'] = numpy.asarray(dates)
            dic['sold_list'] = numpy.asarray(sold_list)
            df = pandas.DataFrame(data=dic)
            res = str(df.head())  
            #return jsonify(df=res)                                                                                                                                           #convert sold list to dataframe
            preds, test_data, future_prediction = logic.train_model(df)
            preds_list.append(preds)
            test_data_list.append(test_data)
            future_prediction_list.append(future_prediction)
        return jsonify(preds=preds_list, test_data=test_data_list, future_prediction=future_prediction_list)
    else:
        return Response(status=404, mimetype="application/json")

    

#########################################################################################################
#item functions
#########################################################################################################

# get item prices given the customer and the item
@app.route("/customer/<customer_id>/item/<item_id>", methods=["GET"])
def get_item_price(customer_id, item_id):
    try:
        customerId = str(customer_id)
        itemId = str(item_id)
        date_range = (1, 4)
    except:
        return Response(status=400, mimetype="application/json")
    dates, purchased_list, sold_list, stock_list, price_list=firebase_access.get_data(collection_id, customerId, itemId, date_range)
    if(dates == []):
        return Response(status=404, mimetype="application/json")
    else:
        return jsonify(customerid=customerId, itemid=itemId, dates=dates, purchased_list=purchased_list, sold_list=sold_list, stock_list=stock_list, price_list=price_list)

# get item details given the customer and the item
@app.route("/customer/<customer_id>/item", methods=["GET"])
def get_item_list(customer_id):
    try:
        customerId = str(customer_id)
    except:
        return Response(status=400, mimetype="application/json")
    company_info = firebase_access.get_company_info(collection_id, customerId)
    if(company_info != None):
        item_list=firebase_access.get_items_list(collection_id, customerId)
        current, predicted, purchased = [], [], []
        for item in item_list:
            dates, purchased_list, sold_list, stock_list, price_list = firebase_access.get_data(collection_id, customerId, item, (939, 999))
            current.append(stock_list[-1])
            predicted_list = firebase_access.return_future_prediction(item)
            predicted.append(predicted_list[-1])
            purchased.append(predicted_list[-1] - stock_list[-1])
        res = {'customerid':customerId, 'item_list':item_list, 'current':current, 'predicted': predicted, 'purchased': purchased}
        return res
    else:
        return Response(status=404, mimetype="application/json")
#########################################################################################################
#customer functions
#########################################################################################################

#provides company details
@app.route("/customer/<customer_id>/", methods=["GET"])
def get_user_details(customer_id):
    try:
        customerId = str(customer_id)
    except:
        return Response(status=400, mimetype="application/json")
    company_info = firebase_access.get_company_info(collection_id, customerId)
    if (company_info != None):
        return jsonify(company_info)
    else:
        return Response(status=404, mimetype="application/json")

if __name__ == '__main__':
    #db.create_all()
    app.run(host='0.0.0.0', port=8080,debug=True)