import pandas as pd
import numpy as np
import keras
from keras import Sequential, optimizers
from keras.layers import LSTM, Dropout, Dense

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

from matplotlib import pyplot as plt

def train_model(df):
    WINDOW_SIZE = 60
    BATCH_SIZE = 10
    LEARNING_RATE = 0.0001
    EPOCHS = 50

    data = df.iloc[:,1:2].values
    train_data, test_data = train_test_split(data, train_size=0.8, shuffle=False)

    sc = MinMaxScaler(feature_range = (0, 1))
    data_scaled = sc.fit_transform(train_data)

    X_train = []
    y_train = []
    for i in range(WINDOW_SIZE, data_scaled.shape[0]):
        X_train.append(data_scaled[i-WINDOW_SIZE:i, 0])
        y_train.append(data_scaled[i, 0])
    X_train, y_train = np.array(X_train), np.array(y_train)

    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

    model = Sequential()
    model.add(LSTM(100, input_shape=(X_train.shape[1], 1)))
    model.add(Dropout(rate=0.5))
    model.add(Dense(20, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))
    optimizer = optimizers.RMSprop(lr=LEARNING_RATE)
    model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['mae'])
    history = model.fit(X_train, y_train, epochs=EPOCHS)

    # plt.figure()
    # plt.plot(history.history['loss'])
    # plt.title('Model loss')
    # plt.ylabel('Loss')
    # plt.xlabel('Epoch')
    # plt.show()

    inputs = data[len(data) - len(test_data) - WINDOW_SIZE:]
    inputs = inputs.reshape(-1, 1)
    inputs = sc.transform(inputs)
    X_test = []
    for i in range(WINDOW_SIZE, inputs.shape[0]):
        X_test.append(inputs[i-WINDOW_SIZE:i, 0])
    X_test = np.array(X_test)
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
    preds = model.predict(X_test)
    preds = sc.inverse_transform(preds)

    # plt.plot(test_data, color = 'black', label = 'actual')
    # plt.plot(preds, color = 'green', label = 'predicted')
    # plt.xlabel('Time')
    # plt.ylabel('Inventory')
    # plt.legend()
    # plt.show()

    pred = inputs[260 - WINDOW_SIZE:260, 0]
    pred = np.array(pred)
    pred = np.reshape(pred, (1, pred.shape[0], 1))
    pred.shape

    future_prediction = model.predict(pred)
    future_prediction = sc.inverse_transform(future_prediction)

    return preds, test_data, future_prediction