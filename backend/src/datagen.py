import requests
import json
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

def generate_fake_data():
	url = "https://gateway-staging.ncrcloud.com/catalog/items/snapshot"
	headers = {
	    'Content-Type': "application/json",
	    'Accept': "application/json",
	    'nep-application-key': "8a00860b6641a0ae0166471356ba000f",
	    'nep-organization': "ncr-market",
	    'nep-service-version': "2.2.1:2",
	    'Authorization': "Basic YWNjdDpqYW1AamFtc2VydmljZXVzZXI6MTIzNDU2Nzg=",
	    'Cache-Control': "no-cache",
	    'Postman-Token': "918d01ae-25a6-4b13-9ec1-83b1d86c6571"
	    }
	response = requests.request("GET", url, headers=headers)

	ncr_items = json.loads(response.text)["snapshot"]

	item_list = []
	for item in ncr_items:
	    item_list.append(item['longDescription']['values'][0]['value'])

	days = np.arange(0, 1000) # 1000 days ~3 years
	data_dict = {'Days': days}
	for item in item_list:
	    base = 50 + np.random.random()*50
	    period = 5 + np.random.random()*10
	    phase = np.random.random()*period
	    sine_amp = (0.7-np.random.random()*0.3)*base
	    random_amp = (0.2-np.random.random()*0.15)*base
	    data_dict[item] = np.sin(days/period + phase) * sine_amp + np.random.random(days.shape) * random_amp + base
	df = pd.DataFrame(data=data_dict)

	return df