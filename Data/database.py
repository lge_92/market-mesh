import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json

import logic
import datagen
import pandas as pd

# Use the application default credentials
cred = credentials.ApplicationDefault()
firebase_admin.initialize_app(cred, {
    'projectId': 'ncr-global-hackathon-2019',
})

db = firestore.client()

df = datagen.generate_fake_data()
preds, test_data, future_prediction = logic.train_model(df)

doc_ref = db.collection("Walmart(dummy-data)").document("string cheese")

doc_ref.set({
	"preds": preds,
	"test_data": test_data,
	"future_prediction": future_prediction
	})